---
Week: 40
Content:  P1P2 Consolidation 1/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 40

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* POC phase video published and linked to on hack.md
* Temperature sensor working and documented with guide on gitlab


### Learning goals
* Temperature sensor
  * Level 1: Students can set up the temperature sensor on a Raspberry Pi
  * Level 2: Students can change the program example to output selected values
  * Level 3: Students can document with a step by step guide

* Python programming
  * Level 1: Students can collaborate to produce a flow chart describing a Python program
  * Level 2: Students can combine code examples
  * Level 3: Students can implement functions

## Deliverables

* 10 min. mandatory weekly meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Group morning meetings |
| 10:00 | Teacher meetings - remember to book a time slot |
| 10:00 | You work on exercises |
| 12:15 | Q&A session - write your question on the whiteboard |
| 12:45'ish | You work on exercises
| 16:15 | end of the day |


## Hands-on time

For details on exercises please see the exercise document: [https://eal-itt.gitlab.io/20a-itt1-project/exercises/](https://eal-itt.gitlab.io/20a-itt1-project/exercises/)

### Exercise 0 - Proof of concept video

...

### Exercise 1 - DS18B20 temperature sensor

...

## Exercise 2 - Combining parts

...

## Comments
