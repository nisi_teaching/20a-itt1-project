---
Week: 01
Content: P2P3 Make it useful for the user 1/2
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 01

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Complete OLA12 material
* Complete gitlab pages

### Learning goals
* Completing project
  * The student can plan the completion of a project
  * The student can prioritize tasks related to completion
  * The students can complete the project with required documentation

## Deliverables

* Optional status meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Feedback on an accomplished task
    2. Help needed
    3. AOB

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. AOB

## Schedule

### Monday  

Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Status meetings - Book a time if you need help or feedback (optional) |
| 9:30 | Hands on time | 
| 12:15 | Lunch |
| 16:15 | end of the day |


## Hands-on time


### Exercise 0 - Continue working on OLA12  

...

### Exercise 1 - Complete gitlab pages

...

## Comments
