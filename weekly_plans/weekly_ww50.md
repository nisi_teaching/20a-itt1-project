---
Week: 50
Content: P2P2 Consolidation 2/3
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 50

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Gitlab pages running

### Learning goals
* Gitlab pages
  * The student knows what gitlab pages is 
  * The student can build webpages with mkdocs
  * The student can configure gitlab pages

## Deliverables

* Optional status meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Feedback on an accomplished task
    2. Help needed
    3. AOB

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. AOB

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:15 | Semester survey feedback |
| 10:00 | Status meetings - Book a time if you want (optional) |
| 12:15 | Lunch |
| 16:15 | end of the day |


## Hands-on time

### Exercise 0 - Gitlab pages

...

## Comments