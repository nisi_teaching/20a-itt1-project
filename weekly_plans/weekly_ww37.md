---
Week: 37
Content:  P1P1 Proof of concept 1/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 37

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* pre mortem completed
* project plan draft

### Learning goals
* Basic gitlab project management
  * Level 1: Know what gitlab.com is in a project management context.
  * Level 2: Able to set up and use issues in gitlab.com
  * Level 3: Able to use gitlab.com for project management

## Deliverables
* Gitlab project created
* Project plan draft created and included in gitlab project
* Updated project plan included in Gitlab project

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:45 | Work on exercises |
| 12:15 | Q&A session - write your question on the whiteboard |
| 12:45'ish | You work on exercises
| 16:15 | end of the day |

## Hands-on time

For details on exercises please see the exercise document: [https://eal-itt.gitlab.io/20a-itt1-project/exercises/](https://eal-itt.gitlab.io/20a-itt1-project/exercises/)

### Exercise 0 - gitlab setup

### Exercise 1 - pre mortem

### Exercise 2 - project plan

### Exercise 3 - Riot/element

## Comments

* In project lectures you work in the teams established at collaboration days
