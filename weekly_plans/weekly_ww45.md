---
Week: 45
Content:  P1P3 Make it useful for the user 2/2
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 45

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Recreate another team's system
* Hand in OLA11 on wiseflow

### Learning goals
* Recreate system
  * Level 1: The student can follow a guide
  * Level 2: The student can follow a guide and create relevant issues.
  * Level 3: The student can follow a guide, create relevant issues and contribute to solutions.

## Deliverables

* 10 min. mandatory weekly meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | You work on exercise 0 |
| 12:15 | Lunch |
| 13:00 | You work on exercise 0 and OLA |
| 16:15 | end of the day |

## Hands-on time

For details on exercises please see the exercise document: [https://eal-itt.gitlab.io/20a-itt1-project/exercises/](https://eal-itt.gitlab.io/20a-itt1-project/exercises/)

### Exercise 0 - Recreate 2 other team's system

...

## Comments

OLA11 details and hand in deadline can be seen on wiseflow
