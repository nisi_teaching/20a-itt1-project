---
Week: 48
Content: P2P1 Proof of concept 3/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 48

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Complete POC wrapup requirements

### Learning goals
* Project management
  * Students know the importance of keeping deadlines and milestones
  * Students can work towards deadlines and milestones
  * Students can define deadlines and milestones

## Deliverables

* Optional status meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Feedback on an accomplished task
    2. Help needed
    3. AOB

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:10 | Status meetings - Book a time if you want (optional) |
| 12:15 | Lunch |
| 16:15 | end of the day |


## Hands-on time

### Exercise 0 - POC wrapup

...

### Exercise 1 - Feedback

...

## Comments