---
Week: 41
Content:  P1P2 Consolidation 2/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 41

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* (none)

### Learning goals
* Documentation
  * Level 1: The student knows how to organize documentation
  * Level 2: The student can write step by step guides
  * Level 3: The student can organize documentation with a meaningful overview

* Design of enclosure
  * The student knows what a high level diagram is
  * The student can describe a system with a high level diagram
  * The student can plan an enclosure for a system including details of the system

* Self assesment
  * The student knows the importance of doing self assesment
  * The student can plan and carry out self assesment with guidance
  * The student can plan and carry out self assesment without guidance

## Deliverables

* 10 min. mandatory weekly meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Group morning meetings |
| 10:00 | Teacher meetings - remember to book a time slot |
| 10:00 | You work on exercises - start with exercise 0 - self assesment quiz |
| 12:15 | Lunch | 
| 13:00 | You work on exercises
| 13:45 | Exercise 4 - Self assesment part 2 
| 14:00 | Q&A online at [https://meet.jit.si/20a-itt1-project](https://meet.jit.si/20a-itt1-project)
| 16:15 | end of the day |


## Hands-on time

For details on exercises please see the exercise document: [https://eal-itt.gitlab.io/20a-itt1-project/exercises/](https://eal-itt.gitlab.io/20a-itt1-project/exercises/)

### Exercise 0 - Self assesment part 1

...

### Exercise 1 - Design of enclosure

...

### Exercise 2 - Documentation

...

### Exercise 3 - Fablab workshops planning

...

### Exercise 4 - Self assesment part 2  

...

## Comments

