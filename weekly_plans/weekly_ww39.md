---
Week: 39
Content:  P1P1 Proof of concept 3/3
Material: See links in weekly plan
Initials: NISI/MON
---

# Week 39 - tentative

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Connection to thingspeak established and documented
* Led/button module tests completed and documented
* Shared code on gitlab
* SSH agent forwarding working and able to interact with gitlab from Raspberry Pi

### Learning goals

* Cloud communication
  * Level 1: The student knows what a cloud IoT service is
  * Level 2: The student can send data to a cloud IoT service using a preconfigured example
  * Level 3: The has knowledge about API communication and HTTPS requests

## Deliverables

* 10 min. mandatory weekly meeting with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Team morning meeting  
  * Generic agenda (feel free to adapt):
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.
    4. Aob

## Schedule

### Monday

| Time | Activity |
| :---: | :--- |
| 9:00 | Introduction |
| 9:30 | Group morning meetings |
| 10:00 | Teacher meetings - remember to book a time slot |
| 10:00 | You work on exercises |
| 12:15 | Q&A session - write your question on the whiteboard |
| 12:45'ish | You work on exercises
| 16:15 | end of the day |


## Hands-on time

For details on exercises please see the exercise document: [https://eal-itt.gitlab.io/20a-itt1-project/exercises/](https://eal-itt.gitlab.io/20a-itt1-project/exercises/)

### Exercise 0 - Thingspeak connect

...

### Exercise 1 - led/button module test

...

### Exercise 2 - SSH keys agent forwarding

...

### Exercise 3 - Contributing code on gitlab

...

## Comments

* Morten is absent at the beginning of the day
* The study start test 2nd attempt is at 13:00 - only valid for students who did not pass 1st attempt.
