![Build Status](https://gitlab.com/EAL-ITT/20a-itt1-project/badges/master/pipeline.svg)


# 20A-ITT1-PROJECT

weekly plans, resources and other relevant stuff for courses.

<h3 align="center">Useful links</h1>
<p><strong>[Course gitlab pages](https://eal-itt.gitlab.io/20a-itt1-project/)</strong></p>  
<p><strong>[Discover IoT website](https://eal-itt.gitlab.io/discover-iot/)</strong></p>
<p><strong>[Discover IoT gitlab project](https://gitlab.com/EAL-ITT/discover-iot/)</strong></p>  
<p><strong>[Students gitlab project group](https://gitlab.com/20a-itt1-project)</strong></p> 
