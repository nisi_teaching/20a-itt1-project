---
title: '20A ITT1 Project'
subtitle: 'OLA11 2nd attempt'
filename: '20A_ITT1_PROJECT_OLA_11'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: 2020-11-05
email: 'mon@ucl.dk'
left-header: \today
right-header: OLA11
skip-toc: false
---

# Introduction

In this obligatory learning activity you will recreate 2 other teams minimum system, using the documentation that the teams has produced.  

If you have problems during the exercise you will report these problems through the other teams gitlab, by creating issues with a label named `bug`.

The checklist's including comments and issue list needs to be handed in through wiseflow as a group handin.

In this 2nd OLA11 attempt you can pick any 2 teams to recreate.
Be aware that other teams might be delayed in answering because this is 2nd attempt.

# Instructions

1. Create a `bug` label in your gitlab project.
2. Hand over your documentation to the teams aasigned to your team (list above).

    You need to make it obvious where to find the documentation, e.g. in a top-level `readme` file.

3. Start recreating the other team's minimum system from the documentation you have received.
4. Report problems through the other teams gitlab by creating issues with a label named bug

\pagebreak

## While recreating  

Make a checklist called `recreation-<other teamname>-<your teamname>.md` in the root of your gitlab project.
The checklist should reflect the system that you are recreating !

The content should be 2 checklist's in this format (one list for each team you have recreated):

```
Recreation checklist for team <other team's name>
===================================================

Team name: <your teamname>

Checklist
------------

- [ ] Project plan completed
- [ ] System block diagram completed
- [ ] Recreate documentation clear and understandable
- [ ] ADC communicating with RPi
- [ ] Temperature sensor communicating with RPi
- [ ] BTN/LED board communicating with RPi
- [ ] Data from RPi sent to Thingspeak


Comments
-----------

<add a bulleted list of bug issues you have created on the projects of other team's>

<Any comments that may be relevant for you, other team's or teachers.  
(Could be that something is super succesful)>
```

\pagebreak

# Follow up 

After hand-in, the lecturers will review the handed in documents.  

Results are communicated through wiseflow.