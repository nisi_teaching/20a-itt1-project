---
Week: 39
tags:
- Thingspeak
- SSH
- API
- GIT
- Gitlab
- Virtual environment
---


# Exercises for ww39

## Exercise 0 - Thingspeak connect

### Information

This exercise will enable you to setup Thingspeak and send data to a Thingspeak channel

### Exercise instructions

1. Get an overview of Thingspeak [https://thingspeak.com/](https://thingspeak.com/)
2. Complete the guide at [https://eal-itt.gitlab.io/discover-iot/cloud/thingspeak-connect](https://eal-itt.gitlab.io/discover-iot/cloud/thingspeak-connect)
3. Document your working thingspeak channel and python application with screenshots in your gitlab project

## Exercise 1 - led/button module test

We want to be able to test our led/button board using the raspberry pi.

### Information

Raspberry Pi has I/O pins for input and output, these are called GPIO.

### Exercise instructions

1. Go to the companion site and get introduced to GPIO at [https://eal-itt.gitlab.io/discover-iot/rpi/GPIO](https://eal-itt.gitlab.io/discover-iot/rpi/GPIO)
2. Draw a diagram of you raspberry pi detailing how you will connect you LEDs and your buttons.

    Ensure that you note the correct pin numbers.

3. Use the examples from the raspberry documentation to create a program that tests both LEDs and the buttons.
4. Put documentation and program in your gitlab repo.

## Exercise 2 - SSH keys agent forwarding

### Information

To be able to pull, push etc. to/from gitlab on your Raspberry Pi (or any other remote machine) you need to forward your SSH private key with the SSH agent.

### Exercise instructions

Go to [https://eal-itt.gitlab.io/discover-iot/misc/ssh-agent-forward](https://eal-itt.gitlab.io/discover-iot/misc/ssh-agent-forward) and complete the instructions.

Note! This requires that you have already set up SSH keys and that your public key is set up on the RPi and Gitlab.

## Exercise 3 - Contributing code on gitlab

### Information

We will do a merge request.

### Exercise instructions

1. Go to the compnaions site for an introduction. The page is at [https://eal-itt.gitlab.io/discover-iot/project_management/mergerequests](https://eal-itt.gitlab.io/discover-iot/project_management/mergerequests)

1. Fork the project [`https://gitlab.com/20a-itt1-project/mr-test-project`](https://gitlab.com/20a-itt1-project/mr-test-project)

    **Fork**, not **clone**. You may want to clone your own repo afterwards.

2. Add your name to the `README.md` file

3. Send a merge request.

    Assign @moozer or @npesdk to the MR - otherwise nobody will notice that you have made a MR.
