---
Week: 48
tags:
- documentation
- POC
---


# Exercises for ww48

## Exercise 0 - POC wrapup

This week is the last week of the part 2 - proof of concept phase. 
The requirements for this exercise is to make the following available in you teams gitlab project:

* POC Block diagram
* POC Schematic
* build instructions/user guide for developers (short)
* Project plan draft
* Use case description
* Link to the above in the project `readme.md` file

The above is needed to ensure that you are on track and can deliver the required content for the OLA12 at the end of the project in week 3, 2021.

## Exercise 1 - Feedback

Decide if your team need's feedback from lecturers. Use issues and the feedback label for feedback requests.

1. Decide which issue to get feedback from 

    No later than Monday 16:15 decide in your team which completed issue you would like feedback from.

2. Assign label to chosen issue

    Give the issue the label `Feedback`   
    Attention! The label is already defined as a group label and you should use this label, if you make you own label the request is not accepted and exercise 1 is considered not completed.
