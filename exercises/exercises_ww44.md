---
Week: 44
tags:
- documentation
- minimum system
- busy wait
- fablab workshops
- DC motors
- H-bridge
---


# Exercises for ww44

## Exercise 0 - Minimum system

### Information

In week 45 you will recreate another team's system from documentation. The system you need to prepare and implement is a minimum system with 3 sensors and monitoring on Thingspeak.  

The system is very generic and collects all the parts yu have been learning about in part 1 of the project.  

The aim is to have a minimum system running and a recreation guide ready to hand over to another group in week 45.  
You might be able to implement the full system specifications or maybe you only accomplish to implement parts of the requirements.  
The important thing is that you have a system and recreation guide that can be handed over to another team next week!  

### Instructions

1. Read about the system code flow [https://eal-itt.gitlab.io/discover-iot/system/code_flow](https://eal-itt.gitlab.io/discover-iot/system/code_flow)
2. Implement minimum system according to the requirements [https://eal-itt.gitlab.io/discover-iot/system/minimum_system](https://eal-itt.gitlab.io/discover-iot/system/minimum_system)
3. Include your working code in your teams gitlab project
3. Create a step by step recreation guide in your documentation on gitlab, include a link to it in your readme.md

## Exercise 1 - Fablab workshops

2 persons from each team according to plan [https://hackmd.io/@nisi/ByDrxZfIv](https://hackmd.io/@nisi/ByDrxZfIv), goes to fablab workshops.

Fablab workshops start at 8:15 + 12:15.

## Exercise 2 - Motor

### Information

This exercise explains how to connect a DC motor to your system and control it from the RPi

### Instructions

1. Follow the instructions at [https://eal-itt.gitlab.io/discover-iot/electronics/h-bridge](https://eal-itt.gitlab.io/discover-iot/electronics/h-bridge)
