---
Week: 51
tags:
- User manual
- Project completion
---


# Exercises for ww51

## Exercise 0 - User manual

### Information

**Your user needs to know how to use your system**  

A user manual is different from a recreation manual, your user is not technical and needs instructions on how to operate your system.  
You are probably already familiar with user manual, they are the ones you get when buying a new product.  
It might be a comprehensive user manual or it might be a quick start guide with a supplementary comprehensive user manual on the internet.  

### Instructions

1. Read this article on writing user manuals [https://grammar.yourdictionary.com/grammar-rules-and-tips/tips-on-writing-user-manuals.html](https://grammar.yourdictionary.com/grammar-rules-and-tips/tips-on-writing-user-manuals.html)
2. Plan the contents of your user manual using the knowledge from step 1
3. Plan the layout and format (physical size)
4. Create the content:
    * Overview of the system using a drawing and naming buttons, connectors etc.
    * How to operate the system with several step by step guides for each use case.
    * A troubleshooting section
    * A link to online ressources (your gitlab pages from week 50)
5. Include the user manual in your gitlab project
6. Link to the user manual from your gitlab pages

## Exercise 1 - Prepare presentations and hand-in (OLA12)

### Information

In week 2 you will present what you have accomplished during the phases from part 2 of the project.
The presentation in week 2 is part of OLA 12. Besides the presentation OLA12 includes a hand-in.

OLA12 includes some work from you. You are strongly adviced to start planning and working on OLA12 already this week to be able to complete the OLA.

### Insrtuctions

The requirements for OLA 12 is described here: [https://eal-itt.gitlab.io/20a-itt1-project/other-docs/20A_ITT1_PROJECT_OLA_12.html](https://eal-itt.gitlab.io/20a-itt1-project/other-docs/20A_ITT1_PROJECT_OLA_12.html)
