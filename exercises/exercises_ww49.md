---
Week: 49
tags:
- RPi images
- Test
---


# Exercises for ww49

## Exercise 0 - RPi images

As part of the recreation, documentation and test of your system, you will make you own custom raspberry pi image.

1. Find the test program you have made previously to test your hardware.

2. Update it to match the hardware you have chosen for you specific system

3. Assume that the test program is in a known location on the pi, make a guide (perhaps a `readme.md` file) to describe how to connect hardware and to run the tests.

4. Fork the [raspberry pi static image project](https://gitlab.com/moozer/raspberry-image-static)

5. Update according to [the docs](https://gitlab.com/moozer/raspberry-image-static/-/blob/master/readme.md)

    That includes setting up networking, installing python packages, configuring and downloading the test program.

6. Generate the image.

    Either do this from the command line or have gitlab do it for you.

7. Test that both the image and the test procedure (from point 4 above) work as intended.
