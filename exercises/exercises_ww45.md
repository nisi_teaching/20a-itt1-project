---
Week: 45
tags:
- documentation
- System recreation
---


# Exercises for ww45

## Exercise 0 - Recreate another team's system

### Information

In this exercise you will recreate 2 other teams system using the documentation that the other team has produced.

If you have problems during the exercise you will report these problems through the other teams gitlab, by creating issues with a label named `bug`.

This is a Obligatory Learning Activity (OLA)  
The checklist including comments and issue list needs to be handed in through wiseflow as a group handin.

Recreation list:

```
Team A1 recreate teams A2, B2 
Team A2 recreate teams A3, B3
Team A3 recreate teams A4, B4
Team A4 recreate teams A1, B1
Team B1 recreate teams A2, B2
Team B2 recreate teams A3, B3
Team B3 recreate teams A4, B4
Team B4 recreate teams A1, B1
```

### Instructions

1. Create a `bug` label in your gitlab project.
2. Hand over your documentation to the teams aasigned to your team (list above).

    You need to make it obvious where to find the documentation, e.g. in a top-level `readme` file.

3. Start recreating the other team's minimum system from the documentation you have received.
4. Report problems through the other teams gitlab by creating issues with a label named bug

### While recreating  

Make a checklist called `recreation-<other teamname>-<your teamname>.md` in the root of your gitlab project.
The checklist should reflect the system that you are recreating !

The content should be 2 checklist's in this format (one list for each team you have recreated):

```
Recreation checklist for team <other team's name>
===================================================

Team name: <your teamname>

Checklist
------------

- [ ] Project plan completed
- [ ] System block diagram completed
- [ ] Recreate documentation clear and understandable
- [ ] ADC communicating with RPi
- [ ] Temperature sensor communicating with RPi
- [ ] BTN/LED board communicating with RPi
- [ ] Data from RPi sent to Thingspeak


Comments
-----------

<add a bulleted list of bug issues you have created on the projects of other team's>

<Any comments that may be relevant for you, other team's or teachers. (Could be that something is super successfull)>

```
