---
Week: 01
tags:
- Project completion
---


# Exercises for ww01

## Exercise 0 - Continue working on OLA12

### Information

In week 2 you will present what you have accomplished during the phases from part 2 of the project.
The presentation in week 2 is part of OLA 12. Besides the presentation OLA12 includes a hand-in.

Before christmas (Week 51) you started working on the content of OLA12, this week you need to finish it to be ready for the hand in and presentation in week 2 

### Instructions

The requirements for OLA 12 is described here: [https://eal-itt.gitlab.io/20a-itt1-project/other-docs/20A_ITT1_PROJECT_OLA_12.html](https://eal-itt.gitlab.io/20a-itt1-project/other-docs/20A_ITT1_PROJECT_OLA_12.html)

## Exercise 1 - Complete gitlab pages

### Information

In week 50 you were asked to create a webpages hosted on gitlab pages.

### Instructions

Complete the webpage, meaning that the content described in week 50 is completed and available.

The needed content are subpages for:

* Project plan
* Use case
* Block diagram
* Recreation manual
* Documentation of system tests
* User manual
* Casing pictures
* Links

If you, are in doubt about setting up gitlab pages revisit the exercise from week 50.