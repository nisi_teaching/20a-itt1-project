---
Week: 37
tags:
- GIT
- Gitlab
- Project plan
- SSH
---


# Exercises for ww37

## Exercise 0 - GIT and Gitlab

### Information

Throughout the education you will use Git and Gitlab to document and manage projects.
This will teach you an essential industry tool and enable you to revisit your code for exams and other exercises.

Complete this exercise in your team and help each other if needed. The exercise is not completed before all team members have a working git installation, gitlab account and ssh keys

### Exercise instructions

The exercise is a google codelab which guides you through the nessesary steps of setting up Git on your local machine as well as a Gitlab account and your first Gitlab repository.

To complete exercise 1 follow the instructions in the gitlab daily workflow codelab [https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0](https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0)

Upon successfull setup you need to make gitlab project for your team, your gitlab project needs to be in the 20a-itt1-project group [https://gitlab.com/groups/20a-itt1-project](https://gitlab.com/groups/20a-itt1-project). Bring your gitlab usernames in the form `@username` and ask NISI or MON for access to the group before creating the project.

Guide on creating a project is here [https://docs.gitlab.com/ee/user/project/](https://docs.gitlab.com/ee/user/project/)


## Exercise 1 - pre mortem

### Information

Imagine that the project has failed and ask, "what did go wrong" ?  
The team members’ task is to generate plausible reasons for the project’s failure.

### Exercise instructions

### part 1

1. Individually read the article on performng a pre mortem [https://hbr.org/2007/09/performing-a-project-premortem](https://hbr.org/2007/09/performing-a-project-premortem)

In your team use the cooperative learning structure **think-pair-share** [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf#section.5](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf#section.5) to find out what went wrong ?

2. Think (10 minutes)  
    Write down a numbered list containing every reason you can think of for the failure. Including things you ordinarily wouldn’t mention as potential problems, for fear of being impolitic.
3. Pair (5 minutes)  
    Compare your lists and remove possible duplicates
4. Share (10 minutes)  
    read one reason from your pair list and explain the reason for putting it on the list. Continue until all reasons have been recorded in a team list and order the list by probablilty.

### part 2

Include the ordered and numbered list in your gitlab project and share a link to the project plan on element in the `UCL ITT 20A` room


## Exercise 2 - Update project plan

### Information

We have supplied a generic project plan covering part 1 of the project, but you still have to fill in the student parts.
This is considered the first iteration of the project plan, you might need to update it at a later stage in the project.

The project plan can be downloaded at: [https://gitlab.com/EAL-ITT/20a-itt1-project/-/raw/master/docs/no_render/project_plan_for_students.md?inline=false](https://gitlab.com/EAL-ITT/20a-itt1-project/-/raw/master/docs/no_render/project_plan_for_students.md?inline=false)

### Exercise instructions

1. Include the [project plan] in your gitlab project
2. Fill in the parts marked: **TBD: This section must be completed by the students.**
3. Create or update the readme.md file in your gitlab project with team members first and last names. Use this format: `firstname lastname`

Remember to rename the project plan to include your group name

## Exercise 3 - Riot/element

### Information

We will use riot as the primary chat system for online communication. This includes both wen you are physically located in a different room and when the lectures are purely online.

Is it necessary to tell you how to behave correctly on chat medias?

### Exercise instructions

1. Go to [`riot.im`](https://app.element.io/#/welcome)
2. Create a user (pseudonymes are ok)
3. We have a room called `UCL ITT 20A`, to be invited send a direct message to either `@moozer:matrix.org` or `@npesdk:matrix.org`

    Default is that people in the room can invite others, so when one from a group is in the room, please invite the others.
