---
Week: 46
tags:
- documentation
- System design
---


# Exercises for ww46

## Exercise 0 - Part 2 design and plan

This week we begin the next phase, where you are to define your own IoT system from the builing block you have learned about in part 1.  
You are allowed to use different sensor's from the supplied sensor kit.

Requirements for the system in Part 2 are:

* Project plan
* Use case - what problem is the system trying to solve ?
* Minimum 2 sensors/actuators
* Cloud connected
* Block diagram
* Recreation manual
* Documentation of system tests
* User manual
* Casing
* Gitlab pages (a webpage)

1. Define the use case

    "The system enables someone to do something", and you must define who "someone" is and what "something" is.

    You have a user and there is a reason for your user to use your IoT system.

    Examples of IoT systems: 

    * Air quality monitoring
    * Industrial production monitoring
    * Greenhouse control
    * Cooling warehouse monitoring
    * Environmental monitoring
    * Monitoring number of people in a room

2. Make an overview of the main part of the IoT system.

    This is a block diagram with sensors, actuators, processing unit and cloud (Thingspeak) stuff.  
    It must enable other developper to understand how the information flow is in the system.

3. Make a project plan

    You have 8 weeks (including this one) to finish the system.
    Make a list of what you are to do each week of the project.

    In week 48, you are finished with a proof-of-concept system.
    In week 3, you are done with a system that your user can use.

    You must make the list of what you expect to do in each week.

4. Add milestones and issues to gitlab

    Milestones are the 3 phases from Part 2, `Proof of concept`, `Consolidation` and `Make it useful for the user`  

## Exercise 1 - Feedback

In part 2 we would like to give you feedback on accomplished tasks.
For part 2, a requirement is that you give atleast 1 completed issue the `Feedback` label in you gitlab issue board.

1. Decide which issue to get feedback from 

    No later than Monday 16:15 decide in your team which completed issue you would like feedback from.

2. Assign label to chosen issue

    Give the issue the label `Feedback`   
    Attention! The label is already defined as a group label and you should use this label, if you make you own label the request is not accepted and exercise 1 is considered not completed.