---
Week: 50
tags:
- Gitlab pages
---


# Exercises for ww50

## Exercise 0 - Gitlab pages


### Information 

Gitlab offers hosting of static webpages to complement your gitlab project.  
In this exercise you will build a webpage and include it in your team's gitlab project.

The webpage is a public accesible webpage to present your project and it's documentation

The webpage needs to include subpages for:  

* Project plan
* Use case
* Block diagram
* Recreation manual
* Documentation of system tests
* User manual
* Casing pictures
* Links

### Instructions

1. Complete the guide at [https://eal-itt.gitlab.io/discover-iot/cloud/gitlab-pages](https://eal-itt.gitlab.io/discover-iot/cloud/gitlab-pages)
2. Include a link to your project's gitlab pages in your `readme.md` file